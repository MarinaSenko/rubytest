<?php

use yii\db\Migration;

/**
 * Class m200429_163223_create_seed_data
 */
class m200429_163223_create_seed_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'ruby',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('ruby'),
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'access_token' => Yii::$app->getSecurity()->generateRandomString(40),
        ]);

        $this->insert('{{%user}}', [
            'id' => 2,
            'username' => 'garage',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('garage'),
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'access_token' => Yii::$app->getSecurity()->generateRandomString(40),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ['id' => [1, 2]]);
    }
}
