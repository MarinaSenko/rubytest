<?php

use yii\web\View;
use yii\helpers\Url;
use app\models\Task;
use app\models\Project;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;


/**
 * @var View $this
 * @var Project $project
 * @var Task $model
 */


Modal::begin([
    'id' => 'edit-task-' . $model->id,
    'size' => 'modal-md',
    'header' => '<h2>Edit task</h2>',
]);
$form = ActiveForm::begin([
    'action' => Url::to(['/task/update', 'id' => $model->id]),
]);

echo $this->render('_task_form', ['model' => $model, 'form' => $form, 'project' => $project]);

ActiveForm::end();
Modal::end();
