<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Project;
use kartik\form\ActiveForm;

/**
 * @var $this View
 * @var Project[] $projects
 */

$model = new Project();

?>

<div class="text-center">
    <h2>SIMPLE TODO LISTS</h2>
    <h4>FROM RUBY GARAGE</h4>
</div>

<div class="project-block">
    <?php foreach ($projects as $project): ?>
        <?= $this->render('_item', ['project' => $project]) ?>
    <?php endforeach ?>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php $form = ActiveForm::begin([
                'id' => 'project-form',
                'action' => Url::to(['/project/create']),
            ]); ?>

            <?= $form->field($model, 'name', [
                'addon' => [
                    'append' => [
                        'content' => Html::submitButton('<i class="fas fa-plus"></i> Add TODO list', ['class' => 'btn btn-primary', 'name' => 'project-button']),
                        'asButton' => true
                    ]
                ]
            ])->label(false) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>