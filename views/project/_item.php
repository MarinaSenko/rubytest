<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Task;
use app\models\Project;
use kartik\grid\GridView;

/**
 * @var View $this
 * @var Project $project
 */

$model = new Task();

$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'label' => '',
        'contentOptions' => [
            'width' => '5%'
        ],
        'attribute' => 'status',
        'format' => 'raw',
        'value' => static function (Task $model) {
            return Html::checkbox(
                '',
                $model->status === Task::STATUS_COMPLETED,
                [
                    'class' => 'task-done',
                    'data-id' => $model->id,
                    'id' => 'task-done-' . $model->id
                ]
            );
        }
    ],
    [
        'attribute' => 'name',
        'vAlign' => 'middle',
    ],
    [
        'label' => '',
        'contentOptions' => [
            'width' => '15%'
        ],
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function (Task $model) use ($project) {
            echo $this->render('_update_task', ['model' => $model, 'project' => $project]);
            return Html::a('<i class="fas fa-chevron-up"></i><span> </span>', ['/task/up', 'id' => $model->id]) .
                Html::a('<i class="fas fa-chevron-down"></i><span> </span>', ['/task/down', 'id' => $model->id]) .
                Html::a('<i class="fas fa-edit"></i><span> </span>', '#',
                    ['class' => 'edit-task', 'data-id' => $model->id]) .
                Html::a('<i class="fas fa-trash"></i>', ['/task/delete', 'id' => $model->id]);
        }
    ],
];

?>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?= GridView::widget([
                'dataProvider' => $project->getAll(),
                'rowOptions' => static function (Task $model) {
                    if ($model->status !== Task::STATUS_COMPLETED
                        && !empty($model->deadline)
                        && $model->deadline < time()
                    ) {
                        return ['class' => 'danger'];
                    } elseif ($model->status === Task::STATUS_COMPLETED) {
                        return ['class' => 'success'];
                    }
                },
                'columns' => $gridColumns,
                'pjax' => true,
                'containerOptions' => ['style' => 'overflow: auto'],
                'bordered' => true,
                'striped' => false,
                'condensed' => false,
                'responsive' => true,
                'hover' => true,
                'floatHeader' => true,
                'itemLabelSingle' => 'task',
                'itemLabelPlural' => 'tasks',
                'summary' => '',
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<i class="fa fa-calendar" aria-hidden="true"></i><span> ' . $project->name . ' </span><a href="' . Url::to([
                            '/project/delete',
                            'id' => $project->id
                        ]) . '" class="pull-right"><i class="fas fa-trash"></i></a>'
                        . '<a href="#" class="pull-right edit-project" id="edit-project-' . $project->id . '" data-id="' . $project->id . '"><i class="fas fa-edit"></i></a>'
                ],
                'toolbar' => [
                    [
                        'content' => $this->render('_create_task', ['project' => $project]),
                        'options' => ['class' => 'btn-group mr-2']
                    ],
                ],
            ]) ?>
        </div>
    </div>

<?= $this->render('_update_project', ['project' => $project]) ?>

    <br>
    <br>

<?php $js = <<<JS
$(document).ready(function() {
  $('.edit-project').click(function() {
      let id = $(this).data('id');
      $('#modal-project-' + id).modal('show');  
  });
  $('.edit-task').click(function() {
      let id = $(this).data('id');
      $('#edit-task-' + id).modal('show');  
  });
   $('.task-done').change(function() {
    let id = $(this).data('id');
     $.ajax({
            url: '/task/change-status?id=' + id,
            type: 'post',
            dataType: 'json',
            data: $('form#news-form').serialize(),
        });
  });
});
JS;
$this->registerJs($js, View::POS_END);

