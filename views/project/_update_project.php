<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Project;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;

/**
 * @var View $this
 * @var Project $project
 */


Modal::begin([
    'id' => 'modal-project-' . $project->id,
    'size' => 'modal-md',
    'header' => '<h2>Edit project</h2>',
]);

$form = ActiveForm::begin([
    'action' => Url::to(['/project/update', 'id' => $project->id])
]);

echo $form->field($project, 'name')->textInput(['maxlength' => 255]);
echo Html::submitButton('Save', ['class' => 'btn btn-primary']);

ActiveForm::end();
Modal::end();