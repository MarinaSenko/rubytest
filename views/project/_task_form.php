<?php

use yii\web\View;
use yii\helpers\Html;
use app\models\Task;
use app\models\Project;
use yii\bootstrap\ActiveForm;
use trntv\yii\datetime\DateTimeWidget;


/**
 * @var View $this
 * @var Project $project
 * @var Task $model
 * @var ActiveForm $form
 */


echo $form->field($model, 'name')->textInput(['maxlength' => 255]);
echo $form->field($model, 'project_id')->hiddenInput(['value' => $project->id])->label(false);
echo $form->field($model, 'deadline')->widget(DateTimeWidget::class,
    [
        'phpDatetimeFormat' => 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ',
        'clientOptions' => [
            'allowInputToggle' => false,
            'sideBySide' => true,
            'locale' => 'en-Us',
            'widgetPositioning' => [
                'horizontal' => 'auto',
                'vertical' => 'auto'
            ]
        ]

    ]);

echo Html::submitButton('Save', ['class' => 'btn btn-primary']);