<?php

use yii\web\View;
use yii\helpers\Url;
use app\models\Task;
use app\models\Project;
use yii\helpers\Html;
use kartik\form\ActiveForm;

/**
 * @var View $this
 * @var Project $project
 */

$model = new Task();


$form = ActiveForm::begin([
    'action' => Url::to(['/task/create']),
    'options' => ['style' => 'margin: 10px 0']
]);

echo $form->field($model, 'project_id')->hiddenInput(['value' => $project->id])->label(false);
echo $form->field($model, 'name', [
                'addon' => [
                    'append' => [
                        'content' => Html::submitButton('Add task', ['class' => 'btn btn-success', 'name' => 'project-button']),
                        'asButton' => true
                    ],
                    'prepend' => [
                        'content' => '<i class="fas fa-plus"></i>'
                    ],
                ],

            ])->label(false);

ActiveForm::end();
