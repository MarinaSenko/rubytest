<?php

namespace app\controllers;

use Yii;
use Throwable;
use app\models\Task;
use yii\web\Response;
use yii\web\Controller;
use yii\db\ActiveRecord;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Class TaskController
 * @package app\controllers
 */
class TaskController extends Controller
{

    /**
     * @return array|array[]
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
                'denyCallback' => static function () {
                    return Yii::$app->response->redirect(['/site/index']);
                },
            ]
        ];
    }

    /**
     * @return string|Response
     * @throws Throwable
     */
    public function actionCreate()
    {
        $model = new Task();
        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            $errorMessage = 'Error creating task';
            try {
                if (!$model->save()) {
                    Yii::$app->session->setFlash('error', $errorMessage);
                }
            } catch (Throwable $e) {
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        }

        return $this->redirect(['/project/index']);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            try {
                $errorMessage = 'Error updating task';
                if (!$model->save()) {
                    Yii::$app->session->setFlash('error', $errorMessage);
                }
            } catch (Throwable $e) {
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        }

        return $this->redirect(['/project/index']);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id): Response
    {
        try {
            $errorMessage = 'Error deleting task';
            if (!$this->findModel($id)->delete()) {
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        } catch (Throwable $e) {
            Yii::$app->session->setFlash('error', $errorMessage);
        }
        return $this->redirect(['/project/index']);
    }

    /**
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionChangeStatus(int $id)
    {
        /**
         * @var Task $model
         */
        $model = $this->findModel($id);

        try {
            $errorMessage = 'Error change task status';
            if (!$model->changeStatus()) {
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        } catch (Throwable $e) {
            Yii::$app->session->setFlash('error', $errorMessage);
        }

        return $this->redirect(['/project/index']);
    }

    /**
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUp(int $id)
    {
        /**
         * @var Task $model
         */
        $model = $this->findModel($id);

        try {
            $errorMessage = 'Error change task priority';
            if (!$model->upPriority()) {
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        } catch (Throwable $e) {
            Yii::$app->session->setFlash('error', $errorMessage);
        }

        return $this->redirect(['/project/index']);
    }

    /**
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDown(int $id)
    {
        /**
         * @var Task $model
         */
        $model = $this->findModel($id);

        try {
            $errorMessage = 'Error change task priority';
            if (!$model->downPriority()) {
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        } catch (Throwable $e) {
            Yii::$app->session->setFlash('error', $errorMessage);
        }

        return $this->redirect(['/project/index']);
    }

    /**
     * @param $id
     * @return array|ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Task::find()
            ->innerJoin('project', 'project.id = task.project_id')
            ->andWhere(['project.user_id' => Yii::$app->user->id])
            ->andWhere(['task.id' => $id])
            ->one();

        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param ActiveRecord $model
     * @throws Throwable
     */
    private function performAjaxValidation(ActiveRecord $model): void
    {
        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->data = ActiveForm::validate($model);
                Yii::$app->end();
            }
        }
    }
}