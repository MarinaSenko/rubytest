<?php

namespace app\controllers;

use Yii;
use Throwable;
use yii\web\Response;
use app\models\Project;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Class ProjectController
 *
 * @package app\controllers
 */
class ProjectController extends Controller
{

    /**
     * @return array|array[]
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
                'denyCallback' => static function () {
                    return Yii::$app->response->redirect(['/site/index']);
                },
            ]
        ];
    }

    /**
     * @return string
     *
     * @author Marina Senko
     */
    public function actionIndex(): string
    {
        $projects = Project::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->all();

        return $this->render('index', [
            'projects' => $projects,
        ]);
    }

    /**
     * @return string|Response
     * @throws Throwable
     */
    public function actionCreate()
    {
        $model = new Project();
        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            $errorMessage = 'Error creating project';
            try {
                $model->user_id = Yii::$app->user->id;
                if (!$model->save()) {
                    Yii::$app->session->setFlash('error', $errorMessage);
                } else {
                    return $this->redirect(['index']);
                }
            } catch (Throwable $e) {
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);
        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            try {
                $errorMessage = 'Error updating project';
                if (!$model->save()) {
                    Yii::$app->session->setFlash('error', $errorMessage);
                }
            } catch (Throwable $e) {
                Yii::$app->session->setFlash('error', $errorMessage);
            }
            return $this->redirect(['index']);
        }

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionDelete(int $id): Response
    {
        try {
            $errorMessage = 'Error deleting project';
            if (!$this->findModel($id)->delete()) {
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        } catch (Throwable $e) {
            Yii::$app->session->setFlash('error', $errorMessage);
        }
        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return array|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel(int $id)
    {
        $model = Project::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere(['id' => $id])
            ->one();

        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $model
     * @throws Throwable
     */
    private function performAjaxValidation($model): void
    {
        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->data = ActiveForm::validate($model);
                Yii::$app->end();
            }
        }
    }
}