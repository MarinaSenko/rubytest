<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Task
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 * @property string $status
 * @property int $priority
 * @property int $deadline
 *
 * @package app\models
 */
class Task extends ActiveRecord
{

    public const STATUS_NEW = 'new';
    public const STATUS_COMPLETED = 'completed';

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return '{{%task}}';
    }

    /**
     * @return array|array[]
     */
    public function rules(): array
    {
        return [
            [['status'], 'safe'],
            [
                ['deadline'],
                'default',
                'value' => static function () {
                    return date(DATE_ISO8601, strtotime('+ 1 day'));
                }
            ],
            [
                ['priority'],
                'default',
                'value' => function () {
                    return $this->getLowestPriority() + 1;
                }
            ],
            [['deadline'], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],
            [['name', 'status'], 'string'],
            [['project_id', 'name'], 'required'],
        ];
    }

    /**
     * @return array|string[]
     */
    public static function getStatuses(): array
    {
        return [
            static::STATUS_NEW,
            static::STATUS_COMPLETED,
        ];
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param int $timestamp
     * @return $this
     */
    public function setDeadline(int $timestamp): self
    {
        $this->deadline = $timestamp;
        return $this;
    }

    /**
     * @param int $priority
     * @return $this
     */
    public function setPriority(int $priority): self
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @param Task $task
     * @return bool
     * @throws \Throwable
     */
    private function updatePriority(Task $task): bool
    {
        $priority = $this->priority;
        return (bool)$this->setPriority($task->priority)->update(true, ['priority'])
            && $task->setPriority($priority)->update(true, ['priority']);
    }

    /**
     * @return bool|false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function changeStatus()
    {
        $status = $this->status === static::STATUS_NEW
            ? static::STATUS_COMPLETED
            : static::STATUS_NEW;

        return $this->setStatus($status)->update(true, ['status']);
    }

    /**
     * @return int
     */
    public function getLowestPriority(): int
    {
        return (int) static::find()
            ->select('MAX(priority)')
            ->where(['project_id' => $this->project_id])
            ->scalar();
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function upPriority(): bool
    {
        return $this->changePriority('<');
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function downPriority(): bool
    {
        return $this->changePriority('>');
    }

    /**
     * @param string $sign
     * @return bool
     * @throws \Throwable
     */
    private function changePriority(string $sign): bool
    {
        /**
         * @var Task $closest
         */
        $closest = static::find()
            ->where([$sign, 'priority', $this->priority])
            ->andWhere(['project_id' => $this->project_id])
            ->orderBy(['priority' => SORT_DESC])
            ->one();

        if ($closest === null) {
            return true;
        }

        return $this->updatePriority($closest);
    }
}