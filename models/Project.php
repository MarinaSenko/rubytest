<?php

namespace app\models;

use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Project
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 *
 * @property Task[] $tasks
 *
 * @package app\models
 */
class Project extends ActiveRecord
{

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return '{{%project}}';
    }

    /**
     * @return array|array[]
     */
    public function rules(): array
    {
        return [
            [['user_id'], 'safe'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getTasks(): ActiveQuery
    {
        return $this->hasMany(Task::class, ['project_id' => 'id'])->orderBy(['priority' => SORT_ASC]);
    }

    /**
     * @return ArrayDataProvider
     */
    public function getAll(): ArrayDataProvider
    {
        return new ArrayDataProvider([
            'allModels' => $this->tasks,
        ]);
    }
}