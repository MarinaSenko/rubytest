-- get all statuses, not repeating, alphabetically ordered

SELECT DISTINCT(`status`)
FROM task
ORDER BY `status` ASC

-- get the count of all tasks in each project, order by tasks count descending

SELECT project.name, COUNT(task.id) as task_count
FROM task
INNER JOIN project ON project.id = task.project_id
GROUP BY project_id
ORDER BY task_count DESC

-- get the count of all tasks in each project, order by projects names

SELECT project.name, COUNT(task.id) as task_count
FROM task
INNER JOIN project ON project.id = task.project_id
GROUP BY project_id
ORDER BY project.name ASC

-- get the tasks for all projects having the name beginning with "N" letter

SELECT task.id, task.name
FROM task
INNER JOIN project ON project.id = task.project_id
WHERE project.name LIKE BINARY 'N%'
ORDER BY project.name ASC

-- get the list of all projects containing the 'a' letter in the middle of the name,
-- and show the tasks count near each project. Mention that there can exist projects without tasks
-- and tasks with project_id = NULL

SELECT project.id as project_id, project.name as project_name, COUNT(task.id) as task_count FROM project
LEFT JOIN task ON task.project_id = project.id
WHERE project.name LIKE '_%a%_'
GROUP BY project.id, project.name
UNION
SELECT project.id as project_id, project.name as project_name, COUNT(task.id) as task_count FROM project
RIGHT JOIN task ON task.project_id = project.id
WHERE project.name LIKE '_%a%_'
GROUP BY project.id, project.name


-- get the list of tasks with duplicate names. Order alphabetically

SELECT name FROM task
GROUP BY name
HAVING count(id) > 1
ORDER BY name ASC

-- get list of tasks having several exact matches of both name and
-- status, from the project 'Garage'. Order by matches count

Не совсем поняла задание

-- get the list of project names having more than 10 tasks in status 'completed'. Order by project_id

SELECT name FROM project
WHERE id IN (
    SELECT task.project_id
    FROM task
    GROUP BY task.project_id
    HAVING count(task.id) > 10
)
ORDER BY project.id ASC